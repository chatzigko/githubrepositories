//
//  RepositoryDetailsViewModel.swift
//  GitHubRepositories
//
//  Created by Konstantinos Chatzigeorgiou on 11/11/2017.
//  Copyright © 2017 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation
import RxSwift

class RepositoryDetailsViewModel {
    
    // MARK: - Properties -
    
    var repository = Variable<Repository?>(nil)
    
}
