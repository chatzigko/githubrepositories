//
//  Utilities.swift
//  GitHubRepositories
//
//  Created by Konstantinos Chatzigeorgiou on 11/11/2017.
//  Copyright © 2017 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation

class Utilities {
    
    // MARK: - Properties -
    
    static let shared = Utilities()
    let dateFormatter = DateFormatter()
    
    // MARK: - Inits -
    
    init() {
        self.dateFormatter.dateFormat = "YYYY-MM-dd"
    }
    
}
