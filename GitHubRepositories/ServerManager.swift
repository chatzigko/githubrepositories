//
//  ServerManager.swift
//  GitHubRepositories
//
//  Created by Konstantinos Chatzigeorgiou on 06/11/2017.
//  Copyright © 2017 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import RealmSwift

class ServerManager {
    
    // MARK: - Properties -
    
    static let shared = ServerManager()
    
    // MARK: - Methods -
    
    func getRepositories(date: Date) {
        let dateString = Utilities.shared.dateFormatter.string(from: date)
        let url = "https://api.github.com/search/repositories?sort=stars&order=desc&q=created:\(dateString)"
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseObject() { (dataResponse: DataResponse<Repositories>) in
            switch dataResponse.result {
            case .success(let repositories):
                do {
                    let realm = try Realm()
                    
                    let ids = repositories.items?.map({ $0.id }) ?? []
                    let objectsToDelete = realm.objects(Repository.self).filter("NOT id IN %@", ids)
                    
                    try realm.write {
                        realm.delete(objectsToDelete)
                        
                        for repository in repositories.items ?? [] {
                            realm.add(repository, update: true)
                        }
                    }
                } catch let error {
                    print("\(error.localizedDescription)")
                }
            case .failure(let error):
                print("\(error.localizedDescription)")
            }
        }
    }
    
}
