//
//  Repositories.swift
//  GitHubRepositories
//
//  Created by Konstantinos Chatzigeorgiou on 06/11/2017.
//  Copyright © 2017 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Repositories: Mappable {
    
    // MARK: - Properties -
    
    var totalCount: Int = 0
    var incompleteResults: Bool = false
    var items: [Repository]?
    
    // MARK: - Inits -
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    // MARK: - Methods -
    
    func mapping(map: Map) {
        self.totalCount         <- map["total_count"]
        self.incompleteResults  <- map["incomplete_results"]
        self.items              <- map["items"]
    }
    
}
