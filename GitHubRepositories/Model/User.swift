//
//  User.swift
//  GitHubRepositories
//
//  Created by Konstantinos Chatzigeorgiou on 06/11/2017.
//  Copyright © 2017 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class User: Object, Mappable {
    
    // MARK: - Properties -
    
    @objc dynamic var login: String?
    @objc dynamic var id: Int = 0
    @objc dynamic var avatarUrl: String?
    @objc dynamic var gravatarId: String?
    @objc dynamic var url: String?
    @objc dynamic var htmlUrl: String?
    @objc dynamic var followersUrl: String?
    @objc dynamic var followingUrl: String?
    @objc dynamic var gistsUrl: String?
    @objc dynamic var starredUrl: String?
    @objc dynamic var subscriptionsUrl: String?
    @objc dynamic var organizationsUrl: String?
    @objc dynamic var reposUrl: String?
    @objc dynamic var eventsUrl: String?
    @objc dynamic var receivedEventsUrl: String?
    @objc dynamic var type: String?
    @objc dynamic var siteAdmin: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // MARK: - Inits -
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    // MARK: - Methods -
    
    func mapping(map: Map) {
        self.login                  <- map["login"]
        self.id                     <- map["id"]
        self.avatarUrl              <- map["avatar_url"]
        self.gravatarId             <- map["gravatar_id"]
        self.url                    <- map["url"]
        self.htmlUrl                <- map["html_url"]
        self.followersUrl           <- map["followers_url"]
        self.followingUrl           <- map["following_url"]
        self.gistsUrl               <- map["gists_url"]
        self.starredUrl             <- map["starred_url"]
        self.subscriptionsUrl       <- map["subscriptions_url"]
        self.organizationsUrl       <- map["organizations_url"]
        self.reposUrl               <- map["repos_url"]
        self.eventsUrl              <- map["events_url"]
        self.receivedEventsUrl      <- map["received_events_url"]
        self.type                   <- map["type"]
        self.siteAdmin              <- map["site_admin"]
    }
    
}
