//
//  Repository.swift
//  GitHubRepositories
//
//  Created by Konstantinos Chatzigeorgiou on 06/11/2017.
//  Copyright © 2017 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Repository: Object, Mappable {
    
    // MARK: - Properties -
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var fullName: String?
    @objc dynamic var owner: User?
    @objc dynamic var isPrivate: Bool = false
    @objc dynamic var htmlUrl: String?
    @objc dynamic var desc: String?
    @objc dynamic var fork: Bool = false
    @objc dynamic var url: String?
    @objc dynamic var forksUrl: String?
    @objc dynamic var keysUrl: String?
    @objc dynamic var collaboratorsUrl: String?
    @objc dynamic var teamsUrl: String?
    @objc dynamic var hooksUrl: String?
    @objc dynamic var issueEventsUrl: String?
    @objc dynamic var eventsUrl: String?
    @objc dynamic var assigneesUrl: String?
    @objc dynamic var branchesUrl: String?
    @objc dynamic var tagsUrl: String?
    @objc dynamic var blobsUrl: String?
    @objc dynamic var gitTagsUrl: String?
    @objc dynamic var gitRefsUrl: String?
    @objc dynamic var treesUrl: String?
    @objc dynamic var statusesUrl: String?
    @objc dynamic var languagesUrl: String?
    @objc dynamic var stargazersUrl: String?
    @objc dynamic var contributorsUrl: String?
    @objc dynamic var subscribersUrl: String?
    @objc dynamic var subscriptionUrl: String?
    @objc dynamic var commitsUrl: String?
    @objc dynamic var gitCommitsUrl: String?
    @objc dynamic var commentsUrl: String?
    @objc dynamic var issueCommentUrl: String?
    @objc dynamic var contentsUrl: String?
    @objc dynamic var compareUrl: String?
    @objc dynamic var mergesUrl: String?
    @objc dynamic var archiveUrl: String?
    @objc dynamic var downloadsUrl: String?
    @objc dynamic var issuesUrl: String?
    @objc dynamic var pullsUrl: String?
    @objc dynamic var milestonesUrl: String?
    @objc dynamic var notificationsUrl: String?
    @objc dynamic var labelsUrl: String?
    @objc dynamic var releasesUrl: String?
    @objc dynamic var deploymentsUrl: String?
    @objc dynamic var createdAt: String?
    @objc dynamic var updatedAt: String?
    @objc dynamic var pushedAt: String?
    @objc dynamic var gitUrl: String?
    @objc dynamic var sshUrl: String?
    @objc dynamic var cloneUrl: String?
    @objc dynamic var svnUrl: String?
    @objc dynamic var homepage: String?
    @objc dynamic var size: Int = 0
    @objc dynamic var stargazersCount: Int = 0
    @objc dynamic var watchersCount: Int = 0
    @objc dynamic var language: String?
    @objc dynamic var hasIssues: Bool = false
    @objc dynamic var hasProjects: Bool = false
    @objc dynamic var hasDownloads: Bool = false
    @objc dynamic var hasWiki: Bool = false
    @objc dynamic var hasPages: Bool = false
    @objc dynamic var forksCount: Int = 0
    @objc dynamic var mirrorUrl: String?
    @objc dynamic var archived: Bool = false
    @objc dynamic var openIssuesCount: Int = 0
    @objc dynamic var forks: Int = 0
    @objc dynamic var openIssues: Int = 0
    @objc dynamic var watchers: Int = 0
    @objc dynamic var defaultBranch: String?
    @objc dynamic var score: Double = 0.0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // MARK: - Inits -
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    // MARK: - Methods -
    
    func mapping(map: Map) {
        self.id                 <- map["id"]
        self.name               <- map["name"]
        self.fullName           <- map["full_name"]
        self.owner              <- map["owner"]
        self.isPrivate          <- map["private"]
        self.htmlUrl            <- map["html_url"]
        self.desc               <- map["description"]
        self.fork               <- map["fork"]
        self.url                <- map["url"]
        self.forksUrl           <- map["forks_url"]
        self.keysUrl            <- map["keys_url"]
        self.collaboratorsUrl   <- map["collaborators_url"]
        self.teamsUrl           <- map["teams_url"]
        self.hooksUrl           <- map["hooks_url"]
        self.issueEventsUrl     <- map["issue_events_url"]
        self.eventsUrl          <- map["events_url"]
        self.assigneesUrl       <- map["assignees_url"]
        self.branchesUrl        <- map["branches_url"]
        self.tagsUrl            <- map["tags_url"]
        self.blobsUrl           <- map["blobs_url"]
        self.gitTagsUrl         <- map["git_tags_url"]
        self.gitRefsUrl         <- map["git_refs_url"]
        self.treesUrl           <- map["trees_url"]
        self.statusesUrl        <- map["statuses_url"]
        self.languagesUrl       <- map["languages_url"]
        self.stargazersUrl      <- map["stargazers_url"]
        self.contributorsUrl    <- map["contributors_url"]
        self.subscribersUrl     <- map["subscribers_url"]
        self.subscriptionUrl    <- map["subscription_url"]
        self.commitsUrl         <- map["commits_url"]
        self.gitCommitsUrl      <- map["git_commits_url"]
        self.commentsUrl        <- map["comments_url"]
        self.issueCommentUrl    <- map["issue_comment_url"]
        self.contentsUrl        <- map["contents_url"]
        self.compareUrl         <- map["compare_url"]
        self.mergesUrl          <- map["merges_url"]
        self.archiveUrl         <- map["archive_url"]
        self.downloadsUrl       <- map["downloads_url"]
        self.issuesUrl          <- map["issues_url"]
        self.pullsUrl           <- map["pulls_url"]
        self.milestonesUrl      <- map["milestones_url"]
        self.notificationsUrl   <- map["notifications_url"]
        self.labelsUrl          <- map["labels_url"]
        self.releasesUrl        <- map["releases_url"]
        self.deploymentsUrl     <- map["deployments_url"]
        self.createdAt          <- map["created_at"]
        self.updatedAt          <- map["updated_at"]
        self.pushedAt           <- map["pushed_at"]
        self.gitUrl             <- map["git_url"]
        self.sshUrl             <- map["ssh_url"]
        self.cloneUrl           <- map["clone_url"]
        self.svnUrl             <- map["svn_url"]
        self.homepage           <- map["homepage"]
        self.size               <- map["size"]
        self.stargazersCount    <- map["stargazers_count"]
        self.watchersCount      <- map["watchers_count"]
        self.language           <- map["language"]
        self.hasIssues          <- map["has_issues"]
        self.hasProjects        <- map["has_projects"]
        self.hasDownloads       <- map["has_downloads"]
        self.hasWiki            <- map["has_wiki"]
        self.hasPages           <- map["has_pages"]
        self.forksCount         <- map["forks_count"]
        self.mirrorUrl          <- map["mirror_url"]
        self.archived           <- map["archived"]
        self.openIssuesCount    <- map["open_issues_count"]
        self.forks              <- map["forks"]
        self.openIssues         <- map["open_issues"]
        self.watchers           <- map["watchers"]
        self.defaultBranch      <- map["default_branch"]
        self.score              <- map["score"]
    }
    
}
