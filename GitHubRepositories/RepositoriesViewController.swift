//
//  ViewController.swift
//  GitHubRepositories
//
//  Created by Konstantinos Chatzigeorgiou on 06/11/2017.
//  Copyright © 2017 Konstantinos Chatzigeorgiou. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RealmSwift
import RxCocoa
import RxSwift
import RxRealm
import RxASDataSources

class RepositoriesViewController: UIViewController {
    
    // MARK: - Properties -
    
    var searchController = UISearchController(searchResultsController: nil)
    var tableNode: ASTableNode!
    var viewModel = RepositoriesViewModel()
    let disposeBag = DisposeBag()
    var selectedIndexPath: IndexPath?
    var filteredRepositories: Results<Repository>?
    
    let dataSource = RxASTableReloadDataSource<SectionOfRepositories>()
    
    // MARK: - Life cycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "GitHub Repositories"
        
        self.createTableNode()
        
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.definesPresentationContext = true
        self.tableNode.view.tableHeaderView = self.searchController.searchBar
        
        self.searchController.searchBar.rx.text.bind(to: self.viewModel.searchText).disposed(by: self.disposeBag)
        
        self.searchController.searchBar.rx.cancelButtonClicked.subscribe(onNext: {
            self.viewModel.searchText.value = ""
        }).disposed(by: self.disposeBag)
        
        self.dataSource.configureCell = { (_, _, _, repository) in
            let cell = ASTextCellNode()
            cell.text = repository.name ?? ""
            return cell
        }
        
        self.tableNode.rx.itemSelected.subscribe(onNext: { indexPath in
            self.searchController.dismiss(animated: true)
            
            let repositoryDetailsViewController = RepositoryDetailsViewController()
            repositoryDetailsViewController.viewModel.repository.value = self.viewModel.dataSourceRepositories.value[indexPath.row]
            self.navigationController?.pushViewController(repositoryDetailsViewController, animated: true)
        }).disposed(by: self.disposeBag)
        
        self.viewModel.dataSourceRepositories.asObservable().map({ (repositories: [Repository]) -> [SectionOfRepositories] in
            return [SectionOfRepositories(header: "", items: repositories)]
        }).bind(to: self.tableNode.rx.items(dataSource: self.dataSource)).addDisposableTo(self.disposeBag)
        
        ServerManager.shared.getRepositories(date: Date().addingTimeInterval(-60 * 60 * 24))
    }
    
    // MARK: - Methods -
    
    func createTableNode() {
        self.tableNode = ASTableNode()
        self.view.addSubnode(self.tableNode)
        
        self.tableNode.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraints([
            NSLayoutConstraint(item: self.tableNode.view, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.tableNode.view, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.tableNode.view, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.tableNode.view, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            ])
    }
    
}
