//
//  RepositoryDetailsViewController.swift
//  GitHubRepositories
//
//  Created by Konstantinos Chatzigeorgiou on 11/11/2017.
//  Copyright © 2017 Konstantinos Chatzigeorgiou. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RxSwift
import RxASDataSources
import SDWebImage

class RepositoryDetailsViewController: UIViewController {
    
    // MARK: - Properties -
    
    let viewModel = RepositoryDetailsViewModel()
    var tableNode: ASTableNode!
    let dataSource = RxASTableReloadDataSource<SectionOfRepositories>()
    let disposeBag = DisposeBag()
    
    // MARK: - Life cycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.viewModel.repository.value?.name
        
        self.createTableNode()
        
        self.dataSource.configureCell = { (_, _, _, repository) in
            let cell = RepositoryCellNode()
            
            if let avatarUrl = self.viewModel.repository.value?.owner?.avatarUrl, let url = URL(string: avatarUrl) {
                cell.userAvatarNode.style.preferredSize = CGSize(width: 100, height: 100)
                
                SDWebImageManager.shared().imageDownloader?.downloadImage(with: url, options: SDWebImageDownloaderOptions.allowInvalidSSLCertificates, progress: nil, completed: { (image, _, _, _) in
                    cell.userAvatarNode.image = image
                })
            }
            
            if let login = self.viewModel.repository.value?.owner?.login {
                cell.userLoginNode.attributedText = self.getAttributedString(title: "Owner Username", value: login)
            }
            
            if let type = self.viewModel.repository.value?.owner?.type {
                cell.userTypeNode.attributedText = self.getAttributedString(title: "Owner Type", value: type)
            }
            
            if let name = self.viewModel.repository.value?.name {
                cell.nameNode.attributedText = self.getAttributedString(title: "Name", value: name)
            }
            
            if let fullName = self.viewModel.repository.value?.fullName {
                cell.fullNameNode.attributedText = self.getAttributedString(title: "Full Name", value: fullName)
            }
            
            if let desc = self.viewModel.repository.value?.desc {
                cell.descNode.attributedText = self.getAttributedString(title: "Description", value: desc)
            }
            
            if let forks = self.viewModel.repository.value?.forks {
                cell.numberOfForksNode.attributedText = self.getAttributedString(title: "Number of Forks", value: "\(forks)")
            }
            
            if let openIssues = self.viewModel.repository.value?.openIssues {
                cell.openIssuesNode.attributedText = self.getAttributedString(title: "Open Issues", value: "\(openIssues)")
            }
            
            if let gitUrl = self.viewModel.repository.value?.gitUrl {
                cell.gitUrlNode.attributedText = self.getAttributedString(title: "Git URL", value: "\(gitUrl)")
            }
            
            return cell
        }
        
        self.viewModel.repository.asObservable().map({ (repository: Repository?) -> [SectionOfRepositories] in
            if let repository = repository {
                return [SectionOfRepositories(header: "", items: [repository])]
            } else {
                return [SectionOfRepositories(header: "", items: [])]
            }
        }).bind(to: self.tableNode.rx.items(dataSource: self.dataSource)).addDisposableTo(self.disposeBag)
    }
    
    // MARK: - Methods -
    
    func createTableNode() {
        self.tableNode = ASTableNode()
        self.tableNode.view.separatorStyle = .none
        self.view.addSubnode(self.tableNode)
        
        self.tableNode.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraints([
            NSLayoutConstraint(item: self.tableNode.view, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.tableNode.view, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.tableNode.view, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.tableNode.view, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            ])
    }
    
    func getAttributedString(title: String, value: String) -> NSAttributedString {
        let title = NSAttributedString(string: "\(title): ", attributes: [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 16)])
        let value = NSAttributedString(string: value, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16)])
        
        let attributedString = NSMutableAttributedString(attributedString: title)
        attributedString.append(value)
        
        return attributedString
    }
    
}
