//
//  RepositoryCellNode.swift
//  GitHubRepositories
//
//  Created by Konstantinos Chatzigeorgiou on 12/11/2017.
//  Copyright © 2017 Konstantinos Chatzigeorgiou. All rights reserved.
//

import AsyncDisplayKit

class RepositoryCellNode: ASCellNode {
    
    // MARK: - Properties -
    
    var userAvatarNode = ASImageNode()
    var userLoginNode = ASTextNode()
    var userTypeNode = ASTextNode()
    var nameNode = ASTextNode()
    var fullNameNode = ASTextNode()
    var descNode = ASTextNode()
    var numberOfForksNode = ASTextNode()
    var openIssuesNode = ASTextNode()
    var gitUrlNode = ASTextNode()
    
    // MARK: - Inits -
    
    override init() {
        super.init()
        
        self.automaticallyManagesSubnodes = true
    }
    
    // MARK: - Methods -
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let verticalStackSpec = ASStackLayoutSpec.vertical()
        verticalStackSpec.alignItems = .start
        verticalStackSpec.spacing = 5
        verticalStackSpec.children = [self.userAvatarNode, self.userLoginNode, self.userTypeNode, self.nameNode, self.fullNameNode, self.descNode, self.numberOfForksNode, self.openIssuesNode, self.gitUrlNode]
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10), child: verticalStackSpec)
    }
    
}
