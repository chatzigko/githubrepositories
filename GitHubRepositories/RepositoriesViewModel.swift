//
//  RepositoriesViewModel.swift
//  GitHubRepositories
//
//  Created by Konstantinos Chatzigeorgiou on 08/11/2017.
//  Copyright © 2017 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxASDataSources
import Differentiator

class RepositoriesViewModel {
    
    // MARK: - Properties -
    
    var repositories: Results<Repository>
    var dataSourceRepositories = Variable<[Repository]>([])
    var searchText = Variable<String?>("")
    let disposeBag = DisposeBag()
    
    // MARK: - Inits -
    
    init() {
        let realm = try! Realm()
        self.repositories = realm.objects(Repository.self)
        
        Observable.collection(from: self.repositories).subscribe(onNext: { repositories in
            self.dataSourceRepositories.value = repositories.toArray()
        }).disposed(by: self.disposeBag)
        
        self.searchText.asObservable().subscribe(onNext: { string in
            if let string = string, !string.isEmpty {
                self.dataSourceRepositories.value = self.repositories.filter("name contains[cd] '\(string)'").toArray()
            } else {
                self.dataSourceRepositories.value = self.repositories.toArray()
            }
        }).disposed(by: self.disposeBag)
    }
    
}

struct SectionOfRepositories {
    var header: String
    var items: [Repository]
}

extension SectionOfRepositories: SectionModelType {
    typealias Item = Repository
    
    init(original: SectionOfRepositories, items: [Repository]) {
        self = original
        self.items = items
    }
    
}
